FILE = cv-haicao

LATEX = pdflatex
FLAGS = -shell-escape

.PHONY: help
help:
	@echo "make [option]"
	@echo "\tgen     generate pdf"
	@echo "\tclean   clean the working directory"
	@echo "\thelp    print this help"

.PHONY: gen
gen:
	@echo "[] Generating pdf file"
	@$(LATEX) $(FLAGS) $(FILE).tex
	@bibtex $(FILE)
	@$(LATEX) $(FLAGS) $(FILE).tex

.PHONY: clean
clean:
	@echo "[] Clear the directory"
	@rm -rf *.aux *-blx.bib *.bbl *.bcf *.blg *.log *.out *.run.xml
